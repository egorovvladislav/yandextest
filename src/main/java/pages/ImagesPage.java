package pages;

import helpers.DriverHelpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImagesPage extends AbstractPage {

    private final String IMAGE_SIZE_DIV_XPATH = "//div[contains(@class, 'serp-item__meta')]";
    private final String DOWNLOAD_BUTTON_XPATH = "//a[contains(@class, 'sizes__download')]";

    @FindBy(xpath = "//input[contains(@class, 'textinput__control')]")
    private WebElement searchBar;

    @FindBy(xpath = "//div[contains(@class, 'search2__button')]")
    private WebElement searchButton;

    @FindBy(xpath = "//input[contains(@class, 'check-button__control') and contains(@value, 'filter')]")
    private WebElement filtersToggle;

    @FindBy(xpath = "//button[contains(@class, 'filter-color')]")
    private WebElement colorFilterButton;

    @FindBy(xpath = "//label[contains(@class, 'radiobox__radio_color_red')]")
    private WebElement redFilter;

    // создан для того, чтобы определять, когда загрузились картинки с красным фильтром
    @FindBy(xpath = "//a[contains(@class, 'serp-item__link') and contains(@href, '/images/search?icolor=red')]")
    private WebElement redFilterMarker;

    @FindBy(xpath = "//div[contains(@class, 'serp-item_pos_') and position() < 8]")
    private List<WebElement> firstSevenImages;

    @FindBy(xpath = DOWNLOAD_BUTTON_XPATH)
    private WebElement downloadButton;

    @FindBy(xpath = DOWNLOAD_BUTTON_XPATH + "//span[contains(@class, 'sizes__sizes')]")
    private WebElement resolutionSpan;

    @FindBy(xpath = "//a[contains(@aria-label, 'Закрыть')]")
    private WebElement closeButton;

    @FindBy(xpath = "//img[contains(@class, 'preview2__thumb_overlay_yes')]")
    private WebElement imageElementForDownload;

    public ImagesPage(WebDriver driver) {
        super(driver);
        DriverHelpers.reinitPage(driver, this);
    }

    public void inputTextInSearchBar(String text){
        inputValueInField(text, searchBar);
    }

    public void clickSearchButton(){
        searchButton.click();
    }

    public void toggleFilters(){
        filtersToggle.click();
    }

    public void setRedFilter(){
        colorFilterButton.click();
        redFilter.click();
    }

    public long getImageSize(WebElement image){
        long resolution;

        DriverHelpers.hardClick(driver, image);
        DriverHelpers.waitVisible(driver, resolutionSpan);

        String[] sides = resolutionSpan.getText().split("×");
        resolution = Long.parseLong(sides[0]) * Long.parseLong(sides[1]);
        closeButton.click();

        return resolution;
    }

    // получаем элемент, который содержит картинку наибольшего разрешения
    public void clickImageWithMaxResolution(){

        // самый большой подвох во всей программе, при смене цвета новый обьект еще не загрузился, но старый с такими же параметрами еще счиатется видимым
        DriverHelpers.waitVisible(driver, redFilterMarker);

        long maxResolution = 0;
        WebElement elementWithMaxResolution = null;
        for (WebElement imageElement:firstSevenImages) {
            long currentResolution = getImageSize(imageElement);
            if(currentResolution > maxResolution){
                maxResolution = currentResolution;
                elementWithMaxResolution = imageElement;
            }
        }

        DriverHelpers.hardClick(driver, elementWithMaxResolution);
    }

    public void downloadOpenedImage(){
        DriverHelpers.waitVisible(driver, downloadButton);
        String src = imageElementForDownload.getAttribute("src");

        try {
            URL imageURL = new URL(src);
            BufferedImage saveImage = ImageIO.read(imageURL);
            ImageIO.write(saveImage, "png", new File("src/main/resources/" + src.split("/")[2] + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
