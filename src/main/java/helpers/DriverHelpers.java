package helpers;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.MainPage;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class DriverHelpers {

    public static WebDriver firefoxDriverInit(){
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

    public static void reinitPage(WebDriver driver, Object page){
        PageFactory.initElements(driver, page);
    }

    public static void closeSession(WebDriver driver, StringBuffer verificationErrors){
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    public static void waitVisible(WebDriver driver, WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 90);
        wait.until(ExpectedConditions.visibilityOf(element));
    }


    public static void hardClick(WebDriver driver, WebElement element){
        DriverHelpers.waitVisible(driver, element);
        new Actions(driver).moveToElement(element).click().perform();
    }
}
