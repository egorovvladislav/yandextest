import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import steps.BaseSteps;

import static helpers.DriverHelpers.closeSession;
import static helpers.DriverHelpers.firefoxDriverInit;

public class YandexTest {

    private WebDriver driver;

    private BaseSteps baseSteps;
    private StringBuffer verificationErrors = new StringBuffer();
    private String defaultSearchParam = "Самолет";

    @Before
    public void setUp() {
        driver = firefoxDriverInit();
        baseSteps = new BaseSteps(driver);
    }

    @Test
    public void testCase() {
        String searchParam = System.getProperty("searchParam") == null ? defaultSearchParam : System.getProperty("searchParam");
        baseSteps.openYandex();
        baseSteps.openImagesTab();
        baseSteps.imageSearchByText(searchParam);
        baseSteps.setRedFilter();
        baseSteps.selectImageWithMaxResolution();
        baseSteps.downloadOpenedImage();
    }

    @After
    public void tearDown() {
        closeSession(driver, verificationErrors);
    }
}