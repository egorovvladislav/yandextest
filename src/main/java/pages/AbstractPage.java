package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractPage {

    protected WebDriver driver;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
    }

    public void openURL(String url){
        driver.navigate().to(url);
    }

    public void inputValueInField(String value, WebElement field){
        field.click();
        field.clear();
        field.sendKeys(value);
    }

    public WebElement getChildElementWithXpath(String parentClass ,String xpath){
        return driver.findElement(By.xpath("//div[contains(@class, '" + parentClass + "')]" + xpath));
    }

    public boolean checkStringWithRegExp(String string, String regExp){
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(string);
        return m.matches();
    }

    // Проверка загрузки страницы через JS по моему мнению наиболее оптимальная в этой ситуации.
    // Страница сама скажет нам, когда она загружена.
    // Иначе пришлось бы проверять слишком много элементов.
    public void waitLoadPage(){
        Boolean readyStateComplete = false;
        while (!readyStateComplete)
        {
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            readyStateComplete = (executor.executeScript("return document.readyState")).equals("complete");
        }
    }

}
