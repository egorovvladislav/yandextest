package pages;

import helpers.DriverHelpers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends AbstractPage {

    private final String YANDEX_URL = "https://www.yandex.ru";

    @FindBy(id = "text")
    private WebElement searchBar;

    @FindBy(xpath = "//div[contains(@class, 'search2__button')]")
    private WebElement searchButton;

    // выбран самый точный локатор, который скорее всего не будет изменен
    @FindBy(xpath = "//a[@data-id='images']")
    private WebElement imagesButton;

    public MainPage(WebDriver driver) {
        super(driver);
        DriverHelpers.reinitPage(driver, this);
    }

    public void openSystem(){
        openURL(YANDEX_URL);
    }

    public void inputTextInSearchBar(String text){
        inputValueInField(text, searchBar);
    }

    public void clickSearchButton(){
        searchButton.click();
    }

    public void clickImagesTab(){
        imagesButton.click();
    }
}
