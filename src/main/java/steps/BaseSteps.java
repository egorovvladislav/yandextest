package steps;

import org.openqa.selenium.WebDriver;
import pages.ImagesPage;
import pages.MainPage;

public class BaseSteps {

    private MainPage mainPage;
    private ImagesPage imagesPage;

    public BaseSteps(WebDriver driver) {
        mainPage = new MainPage(driver);
        imagesPage = new ImagesPage(driver);
    }

    public void openYandex(){
        mainPage.openSystem();
        mainPage.waitLoadPage();
    }

    public void openImagesTab(){
        mainPage.clickImagesTab();
        imagesPage.waitLoadPage();
    }

    public void imageSearchByText(String text){
        imagesPage.inputTextInSearchBar(text);
        imagesPage.clickSearchButton();
    }

    public void setRedFilter(){
        imagesPage.toggleFilters();
        imagesPage.setRedFilter();
    }

    public void selectImageWithMaxResolution(){
        imagesPage.clickImageWithMaxResolution();
    }

    public void downloadOpenedImage(){
        imagesPage.downloadOpenedImage();
    }
}
